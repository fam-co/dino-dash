import pygame
from random import randint

# Window initializing and size
pygame.init()
winWidth = 750
winHeight = 500
win = pygame.display.set_mode((winWidth, winHeight))
pygame.display.set_caption("Dino Dash")

# Predetermined player values
score = 0
lives = 3
x = 0
y = 440
width = 40
height = 60
player = pygame.Rect(x, y, width, height)
gameDelay = 16
rightVel = 4
leftVel = 4 + 2
jumpVel = 1
jumpHeight = 120
bottomBorder = winHeight - height
rightBorder = winWidth - width
run = True
inAir = False
walls = ()
wallCooldown = randint(1000, 4000)
canSpawn = True
lastWall = 0
paused = False

# Moving death wall class
class CreateWall:
  def __init__(self, _height = 100, _width = 10):
    self.height = _height
    self.vel = 2
    self.width = _width
    self.x = winWidth - self.width
    self.y = winHeight - self.height
    self.color = (255, 0, 0)
    self.inFrame = True
    self.collidedWith = False
    self.rect = pygame.Rect(self.x, self.y, self.width, self.height)

  def render(self):
    pygame.draw.rect(win, self.color, self.rect)

  def advance(self):
    global score
    self.rect = self.rect.move(-(self.vel + (score * 0.2)), 0)
    if self.rect.left < 0 and self.inFrame == True:
      self.inFrame = False
      if self.collidedWith == False:
        score += 1

  def collisionDetection(self, playerRect):
    return self.rect.colliderect(playerRect)

# game keeps looping until you quit or you die
while run and lives >= 0:
  pygame.time.delay(gameDelay) # milliseconds between each frame or while loop

  # button clicks - these events happen only once on press
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      run = False
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_t:
        paused = not paused
        if paused == True:
          pauseStart = pygame.time.get_ticks()
        else:
          pauseTime = pygame.time.get_ticks() - pauseStart
          if canSpawn == False:
            lastWall += pauseTime
      if paused == True: # prevent buffering button inputs while game is paused
        continue
      if event.key == pygame.K_SPACE and inAir == False:
        airLoop = -18
        inAir = True
        lastJump = pygame.time.get_ticks()

  # Game pausing - stops rest of the while from running
  if paused == True:
    pauseFont = pygame.font.SysFont('arial', 32)
    pauseText = pauseFont.render("PAUSED", True, (255, 255, 255))
    pauseRect = pauseText.get_rect(center=(winWidth/2, winHeight/2 - 20))
    win.blit(pauseText, pauseRect)
    pygame.display.update()
    continue

  # Key press events, these repeat while key is held down
  keys = pygame.key.get_pressed()
  if keys[pygame.K_LEFT] and player.left - leftVel >= 0:
    player = player.move(-leftVel, 0)
    # x -= leftVel
  if keys[pygame.K_RIGHT] and player.right + rightVel <= winWidth:
    player = player.move(rightVel, 0)
    # x += rightVel

  # Jump logic
  if inAir:
    player = player.move(0, (jumpVel ** 2) * airLoop)
    airLoop += 1
    if airLoop == 19:
      airLoop = -18
      inAir = False

  # This can be used instead of hard borders to teleport the player to the opposite side when hitting the wall
  # Pac Man style
  # if player.left < 0:
  #   player.right = winWidth
  # if player.right > winWidth:
  #   player.left = 0

  # Wall cooldown timer
  if canSpawn == True:
    wall = CreateWall()
    walls += wall,
    canSpawn = False
    lastWall = pygame.time.get_ticks()
  elif pygame.time.get_ticks() - lastWall > wallCooldown:
    canSpawn = True
    wallCooldown = randint(1000, 4000)

  win.fill((0, 0, 0)) # Fill the screen with black before each new frame

  # Draws on the page - walls
  for wall in walls:
    if wall.inFrame:
      wall.advance()
      wall.render()
      if wall.collisionDetection(player) and wall.color == (255, 0, 0):
        lives -= 1
        wall.color = (255, 255, 255)
        wall.collidedWith = True



  # Draws on the page - player, score, and lives
  pygame.draw.rect(win, (0, 0, 255), player)
  scoreFont = pygame.font.Font('freesansbold.ttf', 32)
  scoreText = scoreFont.render("Score: " + str(score), True, (0, 255, 255))
  win.blit(scoreText, (590, 15, 0, 0))
  for i in range(0, lives):
    pygame.draw.rect(win, (255, 0, 0), ((50 * i) + 20, 20, 35, 35))

  # update display
  pygame.display.update()

# After run ends - print score and exit game
print("Your final score was:", score)
pygame.quit()
